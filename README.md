# Skeletor #
Skeletor is a barebones grid designed to replace the Skeleton grid. The ultimate goal is to be a flex grid with no styling. It does include optional gutters for some basic margins.

### Basic usage: ###

Skeletor uses a 12 column grid and mimics the syntax from Skeleton. Columns must be contained within a row and the `columns` class is required for each grid column (not each responsive class). It's recommended to put `columns` as the first class for clarity.

**Install the Package**

`npm install --save skeletor-grid`

**Import the SCSS or CSS Files Via JS**

`import 'skeletor-grid/src/skeletor.scss'` 

or 

`import skeletor-grid/css/skeletor.css`

**Import the SCSS file into Other SCSS Files**

`@import '~skeletor-grid/src/skeletor'`

**Loading for Browser Environments**

You can grab the latest minified CSS file at https://bitbucket.org/codingcolorado/skeletor-grid/raw/master/css/skeletor.min.css

**Basic Grid**

```
<div class="row">
  <div class="columns six-tiny three-medium four-large">Column Content</div>
</div>
```
**Flex Grid**
```
<div class="flex-row">
  <div class="columns six-tiny three-medium four-large">Column Content</div>
</div>
```

### Prefix List ###

The Prefix determines the width of the column. The prefix can be used on it's own (e.g. `five`) but will be overwritten if a suffix is provided by another class.

```
one-*
two-*
three-*
four-*
five-*
six-*
seven-*
eight-*
nine-*
ten-*
eleven-*
twelve-*
```

### Suffix List ###
The Suffix determines the breakpoints for different widths. Skeletor is designed mobile-first. The smallest breakpoint will be used until a larger one is specified.

```
*-micro, 320px
*-tiny, 425px
*-small, 768px
*-medium, 1024px
*-large, 1280px
*-huge, 1440px
*-gigantic, 1920px
```

### Offsets ###
Grids can be offset by a number of columns, useful for centering things or making column based margins. It's a prefix applied to the regular column syntax. E.g. `offset-six-medium` which will offset this column as if there were six columns to the left of it (at medium resolutions, in this example).

You can also offset the grid as if there were columns on the right. This is useful for flex grids which will always stretch to the width of the screen. E.g. `offset-six-medium-right` if you wanted to offset the current column as if there were six columns to the right. This is not necessary when using the basic grid unless you're wrapping rows.

### Basic Grid vs Flex Grid ###
Skeletor does both styles of grid, each with pros and cons. The type of grid is specified based on the row class used. Either `row` or `flex-row`. A flex row should only have a total of 12 columns. The basic grid can have an unlimited number of columns but may need clearfixed.

**Basic**

* A row can contain more than 12 columns. If it does, it'll wrap to the next line.
* Vertical centering is difficult
* When a row wraps, a cell can be placed in the wrong location based on the heights of previous cells. Putting a `<div class="clearfix"></div>` at the end of the previous row will fix this (just like in bootstrap)

**Flex**

* Easier vertical aligment
* Does not need the clearfix hack
* Does not wrap
* Proportional, it will always stretch to fit the width of the screen. This can be countered with offsets. This also means that you can make grids with as many columns as you want with the limit of a single column being twelve 'units'.

### Responsive Helpers ###
Some basic classes for showing/hiding content based on width are included. They use the same suffixes listed above. The first two show/hide classes will show/hide at the suffix breakpoint and any breakpoint larger. Again, mobile first. The two `*-only` classes will show/hide the content only at that resolution. Each of these classes also have a `*-block` suffix for elements that need `display: block;`. This important if you want to show/hide the clearfix class based on resolution. Additionally, there is a `*-inline` for elements that need `display: inline;`.

```
hide-*
show-*
hide-*-only
show-*-only
```

### Gutters ###
The whole point of skeletor was to provide an easy responsive grid without any excess styling. Gutters are included as an optional class for some quick and easy spacing. It can be applied to individual columns or all the columns of a row. Both types of grid support gutters.
```
<div class="row gutters-all">
  <div class="columns twelve">
</div>

<div class="row">
  <div class="columns twelve gutter">
</div>
```

### Example Usage ###
```
<h1>
    <span>Current size: </span>
    <span class="show-micro-only">Micro</span>
    <span class="show-tiny-only">Tiny</span>
    <span class="show-small-only">Small</span>
    <span class="show-medium-only">Medium</span>
    <span class="show-large-only">Large</span>
    <span class="show-huge-only">Huge</span>
    <span class="show-giant-only">Giant</span>
</h1>
<!-- border added for demonstration purposes -->
<style>
    .columns {
        border: 0.5px solid red;
    }
</style>

<h3>Basic grid:</h3>
<div class="row">
    <div class="columns one-small six-tiny two-large offset-four-small offset-none-medium ">
        <div class="hide-small">six</div>
        <div class="show-small hide-large">one<span class="show-small-only"> offset by four</span></div>
        <div class="show-large">two</div>
    </div>
    <div class="columns six-tiny two-small gutter">
        <div class="hide-small">six with gutters</div>
        <div class="show-small">two with gutters</div>
    </div>
    <div class="columns six-tiny three-small six-medium">
        <div class="hide-small">six</br>six</div>
        <div class="show-small-only">three</br>three</div>
        <div class="show-medium">six</br>six</div>
    </div>
    <div class="clearfix show-small-block"></div>
    <div class="columns six">six</div>
    <div class="clearfix hide-small-block"></div>
    <div class="columns six">six</div>
    <div class="clearfix"></div>
</div>

<h3>Flex grid:</h3>
<div class="flex-row">
    <div class="columns six-small three-medium">
        <div class="hide-medium">six</div>
        <div class="show-medium">three</div>
    </div>
    <div class="columns three-small">three<br/>three</div>
    <div class="columns three-small six-medium">
        <div class="hide-medium">three</div>
        <div class="show-medium">six</div>
    </div>
</div>
<div class="flex-row">
    <div class="columns four-small three-medium offset-two-small">
        <div class="hide-medium">four</div>
        <div class="show-medium">three</div>
    </div>
    <div class="columns three-small offset-three-small-right">three<br/>three</div>
</div>
```